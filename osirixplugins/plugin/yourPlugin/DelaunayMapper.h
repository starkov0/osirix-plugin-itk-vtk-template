//
//  DelaunayMapper.h
//  PluginTemplate
//
//  Created by Pierre Starkov on 08/06/14.
//
//

#import <Foundation/Foundation.h>

#import "OsirixAPI/ROIVolumeView.h"
#import "OsiriXAPI/ViewerController.h"
#import "OsiriXAPI/ROI.h"
#import "OsiriXAPI/ITKSegmentation3D.h"
#import <vtkWarpScalar.h>
#import <vtkSmartPointer.h>
#import <vtkSTLWriter.h>

@interface DelaunayMapper : NSObject
{
    ViewerController *vc;
}

- (vtkMapper*) getMapper:(NSArray*)roiArray;
- (void*) getTestMapper:(NSArray*)roiArray;


@end
