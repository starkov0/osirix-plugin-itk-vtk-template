//
//  Main.h
//  MyPlugin
//
//  Created by pierre starkov on 05.03.14.
//
//

#import <Foundation/Foundation.h>
#import "OsiriXAPI/PluginFilter.h"

@interface MyPlugin : PluginFilter

- (void) initPlugin;
- (long) filterImage:(NSString*) menuName;

@end