//
//  DelaunayMapper.m
//  PluginTemplate
//
//  Created by Pierre Starkov on 08/06/14.
//
//

#import "DelaunayMapper.h"

@implementation DelaunayMapper

-(id) init
{
    self = [super init];
    vc = [ViewerController frontMostDisplayed2DViewer];
    return self;
}

// creates Delaunay vtkMapper for a list of ROIs
- (vtkMapper*) getMapper:(NSArray*)roiArray
{
    // get coutour points from ROIs
    NSMutableArray *ptsArray = [NSMutableArray array];
    for (int i=0; i<[roiArray count]; i++){
        [ptsArray addObjectsFromArray:[self getPointsFromROI:[roiArray objectAtIndex:i] withinImageNumber:i]];
    }
    
    // init vtkPoints
    vtkPoints *points = vtkPoints::New();
    long i = 0;
    for( NSArray *pt3D in ptsArray){
        points->InsertPoint( i++, [[pt3D objectAtIndex: 0] floatValue], [[pt3D objectAtIndex: 1] floatValue], [[pt3D objectAtIndex: 2] floatValue]);
    }
    
    // polydata
    vtkPolyData *profile = vtkPolyData::New();
    profile->SetPoints( points);
    
    // delaunay
    vtkDelaunay3D *delaunay3D = vtkDelaunay3D::New();
    delaunay3D->SetInput(profile);
    
    // mapper
    vtkDataSetMapper *delaunayMapper = vtkDataSetMapper::New();
    delaunayMapper->SetInputConnection(delaunay3D->GetOutputPort());
    
    // delete
    points->Delete();
    profile->Delete();
    delaunay3D->Delete();
    
    return delaunayMapper;
}

-(NSMutableArray*) getPointsFromROI:(ROI*)roi withinImageNumber:(int)imageNumber
{
    // init points
    NSMutableArray *ptsArray = [NSMutableArray array];
    NSMutableArray	*ptsContour = nil;
    
    // get current 2D Viewer
    DCMPix	*curDCM = [[vc pixList:[vc curMovieIndex]] objectAtIndex:imageNumber];
    
    //    if ([roi type] == 20)
    ptsContour = [ITKSegmentation3D extractContour:[roi textureBuffer] width:[roi textureWidth] height:[roi textureHeight] numPoints: 100 largestRegion: NO];
    //    else
    //        points = [roi splinePoints]; // in case it s not a brushROI
    
    // fill points
    for( int i = 0; i < [ptsContour count]; i++)
    {
        float location[ 3];
        [curDCM convertPixX: [[ptsContour objectAtIndex: i] x] pixY: [[ptsContour objectAtIndex: i] y] toDICOMCoords: location pixelCenter: YES];
        
        NSArray	*pt3D = [NSArray arrayWithObjects: [NSNumber numberWithFloat: location[0]], [NSNumber numberWithFloat:location[1]], [NSNumber numberWithFloat:location[2]], nil];
        [ptsArray addObject: pt3D];
    }
    
    return ptsArray;
}

@end
