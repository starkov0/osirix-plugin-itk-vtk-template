Hello,

This repository is intended for Osirix plugin development with the use of VTK and ITK libraries. 
32 and 64 bits libraries are included allowing multiple version development.

The depot is created on October 24th 2014.
In case Xcode is outdated, libraries are not and can be used as presented.

A great thanks to Jan Perhac and David Juan Garcia bellow for providing me with usable VTK ITK libraries.

Start coding at "osirixplugins/plugin/yourPlugin/".

Pierre Starkov